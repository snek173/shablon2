/**$('.slider-wrap').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			centerMode: true,
			variableWidth: true,
			adaptiveHeight: true,
			arrows: false,
			dots: true,
});

$('.row_product1').slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			autoplay: true,
			dotsClass: "sliderUlBody_ul",
			appendDots: $('.row_sliderUlBody'),
			dots: true,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
			}},{
				breakpoint: 600,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}},{
				breakpoint: 480,
				settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}}]
});**/
$(document).ready(function(){
	$(".gosearch.my-search").click(function(e){
    	$(".header_bottom-nav-right-search").toggle(0);
		e.preventDefault();
    });
    $(".rating").starbox({
    	autoUpdateAverage: true,
		ghosting: true
    });
	  $("#owl-slider").owlCarousel({
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		pagination : true,
		singleItem:true,
		autoPlay: true
	  });

	$("#owl-slider2").owlCarousel({
		items : 4,
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		pagination : true,
		itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // betweem 900px and 601px
		itemsTablet: [600,1], //2 items between 600 and 0
		itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
	  });
    $("#owl-slider3").owlCarousel({
		items : 4,
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		pagination : false,
		itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // betweem 900px and 601px
		itemsTablet: [600,1], //2 items between 600 and 0
		itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
	  });
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 1000,
      values: [ 100, 1000 ],
      slide: function( event, ui ) {
		 $( ".blockSum" ).html( "<div class='blockSum1'>" + ui.values[ 0 ] + "$</div><div class='blockSum2'>" + ui.values[ 1 ] +"$</div>");
      }
    });
    $( ".blockSum" ).html( "<div class='blockSum1'>" + $( "#slider-range" ).slider( "values", 0 ) +
      "$</div><div class='blockSum2'>" + $( "#slider-range" ).slider( "values", 1 )  +"$</div>");

	$(".blockLeftPM, .conteinerBlockLeft_name").click(function(e){
		e.preventDefault();
		$(this).closest(".conteinerBlockLeft").children(".conteinerBlockLeft_body").toggleClass("displayblock");

	});
});
